//@linked https://highlightjs.org/
//@linked https://marked.js.org/#/README.md#README.md
var marked = require("marked");
var fs = require("fs");
var highlight = require("highlight");


marked.setOptions({
    renderer: new marked.Renderer(),
    highlight: function (code) {
        return highlight.highlight(code).value;
    },
    pedantic: false,
    gfm: true,
    tables: true,
    breaks: false,
    sanitize: false,
    smartLists: true,
    smartypants: true,
    xhtml: false
});

var css = "<link href=\"http://cdn.bootcss.com/highlight.js/8.0/styles/rainbow.min.css\" rel=\"stylesheet\">\n" +
    // var css = "<link href=\"http://cdn.bootcss.com/highlight.js/8.0/styles/monokai_sublime.min.css\" rel=\"stylesheet\">\n" +
    "<script src=\"http://cdn.bootcss.com/highlight.js/8.0/highlight.min.js\"></script>\n" +
    "<script>hljs.initHighlightingOnLoad();</script>";

// var context = marked('# Marked in browser\n\nRendered by **marked**.');
// console.log(context);
var path = "./doc/1.新手上路/新手上路-gameServer-JavaScript[jsGsStart]";

fs.readFile(path + ".md", "UTF-8", function (err, data) {
    console.log("fs:" + data);
    var marked2 = marked(data);
    console.log("marked:" + marked2);
    fs.writeFile(path + ".html", marked2, function (err) {
        if (err) throw  err;
        console.log("OK");
        fs.appendFile(path + ".html", css, function (err) {
            if (err) throw  err;
            console.log("append css OK")
        });
    });

});


var express = require('express');
var app = express();

app.get('/', function (req, res) {

    fs.readFile(path + ".md", "UTF-8", function (err, data) {
        console.log("fs:" + data);
        var marked2 = marked(data);
        console.log("marked:" + marked2);
        fs.writeFile(path + ".html", marked2, function (err) {
            if (err) throw  err;
            console.log("OK");
            fs.appendFile(path + ".html", css, function (err) {
                if (err) throw  err;
                console.log("append css OK");
                res.send('Hello World!');
            });
        });

    });


});

app.listen(3000, function () {
        console.log('Example app listening on port 3000!');
    }
);