// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({width: 1440, height: 900,frame: true,transparent:true});

    // and load the index.html of the app.
    mainWindow.loadFile('index.html');

    // Open the DevTools.
    mainWindow.webContents.openDevTools();

    mainWindow.setHasShadow(true);

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

const ipc = require('electron').ipcMain;
const dialog = require('electron').dialog;

ipc.on('openDirDialog', function (event,receiveChannel) {
    dialog.showOpenDialog(mainWindow,
        {properties: ['openDirectory', "multiSelections"]}
        ,
        function (files) {
            if (files) event.sender.send(receiveChannel, files);
        }
    )
});

ipc.on('openFileDialog', function (event,receiveChannel) {
    dialog.showOpenDialog(mainWindow,
        {properties: ['openFile']}
        ,
        function (files) {
            if (files) event.sender.send(receiveChannel, files)
        }
    )
});


ipc.on('saveDialog', function (event,receiveChannel) {
    const options = {
        title: '选择保存到文件',
        // filters: [
        //     { name: 'Images', extensions: ['jpg', 'png', 'gif'] }
        // ]
    };
    dialog.showSaveDialog(options, function (filename) {
        event.sender.send(receiveChannel, filename)
    })
});

console.log("[INFO] main "+" init shell");
var process = require('child_process');
ipc.on('shell', function (event,args,receiveChannel) {
    process.exec(args,function (error, stdout, stderr) {
        event.sender.send(receiveChannel,[error,stderr,stderr]);
        // dialog.showMessageBox({
        //     title  : '执行shell'
        //     , type  : 'info'
        //     , message : "args:"+args+'\r\n stdout:'+stdout +"\r\n stderr:"+stderr +"\r\n error:"+error
        // })
    });
});


