function Toast() {

}

Toast.prototype.show = function (msg) {
    let x = document.getElementById("snackBar");
    x.innerText = msg;
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
};

Toast.prototype.showLoading = function () {
    let x = document.getElementById("loading");
    x.className = "show";
};
Toast.prototype.hideLoading = function () {
    let x = document.getElementById("loading");
    x.className = "";

};

var toast = new Toast();
try {
    if (module && module.exports) {
        // module.exports = {
        //     Toast: toast
        // };
        module.exports = toast;
    }
} catch (error) {
    console.log(error);
}
// toast.show("fas");