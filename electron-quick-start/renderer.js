const Toast = require('./js/toast.js');


const ipc = require('electron').ipcRenderer;


const saveBtn = document.getElementById('save-dialog');

saveBtn.addEventListener('click', function (event) {
    ipc.send('save-dialog')
});

ipc.on('saved-file', function (event, path) {
    if (!path) path = '无路径';
    document.getElementById('logview').innerHTML = `${path}`
});


var fs = require('fs');

$("#btn_write").click(function writeFile() {
    console.log(textarea.val());
    var path = document.getElementById('logview').innerHTML;
    fs.writeFileSync(
        path,
        textarea.val(),
        'utf8'
    );
});


$("#btn_ll").click(function ll() {
    var path = document.getElementById('logview').innerHTML;
    var dirTree = "";
    fs.readdirSync(path).forEach(function (ele, index) {
        var info = fs.statSync(path + "/" + ele);
        if (info.isDirectory()) {
            console.log("dir: " + ele);
            dirTree += ele;
            dirTree += ele + "/";
        } else {
            console.log(index + " file: " + ele);
            dirTree += ele;
        }
        dirTree += "\r\n";
    });
    document.getElementById('logview').innerHTML = dirTree;
});

// console.log("[INFO] render " + " send shell");
// ipc.send('shell', "TextureMerger -p d:/M2/M2/mmap.wil d:/M2/M2/mmap.wil -o d:/M2/M2/mmap.wil/mmap.wil.json");

//D:\Program Files\Egret\TextureMerger\TextureMerger.exe
function mulSelectDir(receiveChannel, receiveCallback) {
    ipc.on(receiveChannel,receiveCallback);
    ipc.send('openDirDialog', receiveChannel);

}

function selectFile(receiveChannel, receiveCallback) {
    ipc.on(receiveChannel, receiveCallback);
    ipc.send('openFileDialog', receiveChannel);
}


$("#btn_select_path_texture").click(function () {
    selectFile("path_texture", function (event, paths) {
        console.log("[INFO] path btn_select_path_texture");
        $("#path_texture").val(paths[0]);
    });
});


function windowPath2UnixPath(path){
    return path.split('\\').join('/');
}
$("#btn_select_path_texture_src").click(function () {
    mulSelectDir("path_texture_src", function (event, paths) {
        console.log("[INFO] path btn_select_path_texture_src");
        var path = windowPath2UnixPath(paths[0]);
        $("#path_texture_src" ).val(path);
        var parentDirName = path.substring(path.lastIndexOf("/")+1);
        var parentDir = path.substring(0,path.lastIndexOf("/"));
        var exePath = windowPath2UnixPath($("#path_texture").val());
        var exe= exePath.substring(exePath.lastIndexOf("/")+1);
        var dst = parentDir + "/" + parentDirName + ".json";
        ipc.on("onShellComplete", function (event, args) {
            Toast.show("纹理合并完成:"+JSON.stringify(args));
            $("#path_textures").val(dst);
        });
        ipc.send('shell', exe + " -p " + path + " -o " + dst,"onShellComplete");
        Toast.show("合并纹理中...");
    });
});
$("#btn_create_path_textures_animation").click(function () {
    // Toast.show("生成纹理动画中...");
    // Toast.showLoading();
    // setTimeout(function () {
    //     Toast.hideLoading();
    // },4000);

});